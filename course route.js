//setup the dependencies
const express = require("express");
const router = express.Router();

//import the controller file
const courseController = require("../controllers/course");
const auth = require("../auth");

//route to get all active courses
router.get("/", (req, res)=>{
    courseController.getCourses().then(resultFromController => res.send(resultFromController))
})

//route to get specific course
router.get("/:courseId", (req, res) => {

    //"/:courseId" here is called a "wildcard" and its value is anything that is added at the end of localhost:4000/courses

    //eg. localhost:4000/courses/dog = the value of our wildcard is "dog"

    // console.log(req.params)

    //req.params always given an object. The key for this object is our wildcard, and the value comes from the request 

    courseController.getSpecific(req.params.courseId).then(resultFromController => res.send(resultFromController))

})

//route to create a new course

//when a user sends a specific method to a specific endpoint (in this case a POST req to our /courses endpoint) the code within this route will be run
router.post("/", auth.verify, (req,res) => {
    //auth.verify here is something called "middleware". "Middleware" is any function must first be resolved before any succeeding code will be executed.

    //show in the console the req body
    // console.log(req.body)

    //invoke the addCourse method contained in the courseController module, which is an object. Pages, when imported via Node are treated as objects.

    //We also pass the req.body to addCourse as part of the data that it needs

    //Once addCourse has resolved,. then can send whatever it returns (true or false) as its response

    if(auth.decode(req.headers.authorization).isAdmin){
            courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
    }else{
        res.send(false)
    }
})

//route to update a single course
router.put("/:courseId", auth.verify, (req, res) => {
    courseController.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
})


//ACTIVITY

router.delete("/:courseId", auth.verify, (req, res) => {
    courseController.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
})



//export the router
module.exports = router;

//token admin@email.com
//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMGNlNjgzNmNiZWUyYTQ4MTQ3ZTdiNSIsImVtYWlsIjoiYWRtaW5AZW1haWwuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjQ1MDkxNzAyfQ.E7D4P0JcKscnt2ZwPZT89BJnZYg_8DnCOmjfKVCb9sk

//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMGNlNDc2OGMwOGJhODg1NTdmMThhNiIsImVtYWlsIjoic21hMTAxQGdtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NDUwOTU3NDN9.ePa7Va7tadiyQTyw3YscXRiHdST1Ip79N-8i1dVWA80